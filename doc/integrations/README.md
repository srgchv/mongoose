# Contents

* [Prometheus](#prometheus)
* [Grafana](#grafana)

# Prometheus

https://prometheus.io/

## Deploy

TODO

## Usage

TODO

## Approach

Use the [Prometheus client library](https://github.com/prometheus/client_java) for the Mongoose code instrumentation.

# Grafana

## Deploy

TODO

## Usage

TODO

## Approach

1. Make Grafana able to export the chart into the SVG file.
2. Make Grafana able to show the chart from the beginning.
