| Group                      | Name                         | Version     | Linkage  | License                          | Purpose |
|----------------------------|------------------------------|-------------|----------|----------------------------------|-------------------|
| commons-codec              | commons-codec                | 1.11        | Dynamic  | Apache 2.0                       | Decode hexadecimal update mask
| commons-lang               | commons-lang                 | 2.6         | Dynamic  | Apache 2.0                       | Formatting for the suppliers subsystem
| com.fasterxml.jackson.core | jackson-annotations          | 2.9.5       | Dynamic  | Apache 2.0                       | Logging
| com.fasterxml.jackson.core | jackson-core                 | 2.9.5       | Dynamic  | Apache 2.0                       | Logging
| com.fasterxml.jackson.core | jackson-databind             | 2.9.5       | Dynamic  | Apache 2.0                       | Logging
| com.lmax                   | disruptor                    | 3.4.2       | Dynamic  | Apache 2.0                       | Logging
| org.javassist              | javassist                    | 3.22.0-GA   | Dynamic  | Apache 2.0, LGPL 2.1, MPL 1.1 *  | Net storage driver implementations
| com.github.akurilov        | confuse                      | 1.1.4       | Dynamic  | Apache 2.0                       | Configuration
| com.github.akurilov        | confuse-io-json              | 1.0.3       | Dynamic  | Apache 2.0                       | Configuration \[de]serialization
| com.github.akurilov        | java-commons                 | 2.1.7       | Dynamic  | Apache 2.0                       | Used everywhere
| com.github.akurilov        | fiber4j                      | 1.0.1       | Dynamic  | Apache 2.0                       | Cooperative multitasking support
| com.github.akurilov        | netty-connection-pool        | 1.0.9       | Dynamic  | Apache 2.0                       | Net storage driver implementations
| org.apache.logging.log4j   | log4j-api                    | 2.8.2       | Dynamic  | Apache 2.0                       | Logging
| org.apache.logging.log4j   | log4j-core                   | 2.8.2       | Dynamic  | Apache 2.0                       | Logging
| org.apache.logging.log4j   | log4j-iostreams              | 2.8.2       | Dynamic  | Apache 2.0                       | Logging
| org.apache.logging.log4j   | log4j-jul                    | 2.8.2       | Dynamic  | Apache 2.0                       | Logging
| org.apache.logging.log4j   | log4j-slf4j-impl             | 2.8.2       | Dynamic  | Apache 2.0                       | Logging
| io.dropwizard.metrics      | metrics-core                 | 4.0.3       | Dynamic  | Apache 2.0                       | Metrics accounting
| io.netty                   | netty-buffer                 | 4.1.25.Final| Dynamic  | Apache 2.0                       | Net storage driver implementations
| io.netty                   | netty-common                 | 4.1.25.Final| Dynamic  | Apache 2.0                       | Net storage driver implementations
| io.netty                   | netty-codec-http             | 4.1.25.Final| Dynamic  | Apache 2.0                       | Net storage driver implementations
| io.netty                   | netty-handler                | 4.1.25.Final| Dynamic  | Apache 2.0                       | Net storage driver implementations
| io.netty                   | netty-resolver               | 4.1.25.Final| Dynamic  | Apache 2.0                       | Net storage driver implementations
| io.netty                   | netty-transport              | 4.1.25.Final| Dynamic  | Apache 2.0                       | Net storage driver implementations
| io.netty                   | netty-transport-native-epoll | 4.1.25.Final| Dynamic  | Apache 2.0                       | Net storage driver implementations
| org.slf4j                  | slf4j-api                    | 1.7.25      | Dynamic  | MIT                              | Logging

Notes:

(*) The particular license to use may be chosen from this set. MPL 1.1 - "Mozilla Public License 1.1".
